
package com.example.hausbrandt.pojo;

import java.util.ArrayList;
import java.util.List;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationResponse {

    @SerializedName("notification")
    @Expose
    private ArrayList<NotificationData> notification = null;

    public ArrayList<NotificationData> getNotification() {
        return notification;
    }

    public void setNotification(ArrayList<NotificationData> notification) {
        this.notification = notification;
    }

}
