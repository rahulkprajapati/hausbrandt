package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.adapter.FavouriteAdapter;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.CustomerLevelResponse;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteResponse;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class CustomerLevelActivity extends AppCompatActivity {



    ImageView back;
    Button done;

    TextView week, month, year, level;

    PrefsHelper prefsHelper;
    ImageView notification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_level);

        prefsHelper = new PrefsHelper(this);

    back = findViewById(R.id.back);
    done = findViewById(R.id.done);
    week = findViewById(R.id.week);
    month = findViewById(R.id.month);
    year = findViewById(R.id.year);
    level = findViewById(R.id.level_name);


        get_level();
        back.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            finish();
        }
    });

        notification = findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(CustomerLevelActivity.this, NotificationActivity.class));

            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }


    private void get_level() {
        CommonMethods.GetDialog(this, "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<CustomerLevelResponse> call3 = null;
        //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.get_level(String.valueOf(prefsHelper.getPref(Constants.language)),  token);
        assert call3 != null;
        call3.enqueue(new Callback<CustomerLevelResponse>() {
            @Override
            public void onResponse(Call<CustomerLevelResponse> call, retrofit2.Response<CustomerLevelResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(CustomerLevelActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {


                            month.setText(String.valueOf(response.body().getMonth()));
                            week.setText(String.valueOf(response.body().getWeek()));
                            year.setText(String.valueOf(response.body().getYear()));
                            level.setText(String.valueOf(response.body().getYourLevel()));






                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<CustomerLevelResponse> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }



}