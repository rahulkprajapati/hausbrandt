package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;

public class PasswordReset extends AppCompatActivity implements View.OnClickListener{

    private String temp_id;
    private ImageView back;
    private ApiInterface apiInterface;
    private Button submit_btn;
    private EditText edt_password,edt_con_password;
    private String str_password,str_con_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
        back=findViewById(R.id.back);
        submit_btn=findViewById(R.id.submit_btn);
        edt_con_password=findViewById(R.id.edt_con_password);
        edt_password=findViewById(R.id.edt_password);
        submit_btn.setOnClickListener(this);
        back.setOnClickListener(this);
        temp_id=getIntent().getStringExtra("temp_id");
    }

    private void getText(){
        str_password=edt_password.getText().toString();
        str_con_password=edt_con_password.getText().toString();
    }

    private void  Submit() {
        CommonMethods.GetDialog(PasswordReset.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RegisterPojo> call3 = null;
        call3 = apiInterface.reset_password(temp_id,str_password,str_con_password);
        assert call3 != null;
        call3.enqueue(new Callback<RegisterPojo>() {
            @Override
            public void onResponse(Call<RegisterPojo> call, retrofit2.Response<RegisterPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(PasswordReset.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();
                                startActivity(new Intent(PasswordReset.this,LoginActivity.class));
                                Toast.makeText(PasswordReset.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();

                            } else {

                                Toast.makeText(PasswordReset.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(PasswordReset.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }
            @Override
            public void onFailure(Call<RegisterPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_btn:
                getText();
                if(str_password.equalsIgnoreCase("")){
                    edt_password.setError("Please enter password");
                }else if(str_con_password.equalsIgnoreCase("")){
                    edt_con_password.setError("Please enter password");
                }else if(!str_con_password.equalsIgnoreCase(str_password)){
                    edt_con_password.setError("Confirm password not matched");
                }else {
                    Submit();
                }
                break;
            case R.id.back:
                finish();
                break;
        }
    }
}