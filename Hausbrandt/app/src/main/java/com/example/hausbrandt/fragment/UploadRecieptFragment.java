package com.example.hausbrandt.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.example.hausbrandt.R;
import com.example.hausbrandt.activity.CustomerLevelActivity;
import com.example.hausbrandt.activity.NotificationActivity;
import com.example.hausbrandt.activity.ReferAndEarnActivity;
import com.example.hausbrandt.adapter.FavouriteAdapter;
import com.example.hausbrandt.adapter.ShopListAdapter;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.model.ShopNearYouModel;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.pojo.ShopNearYouPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;
import com.google.zxing.Result;
import com.hbb20.CountryCodePicker;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static com.example.hausbrandt.fragment.EditProfileActivity.CAMERA_REQUEST;
import static com.example.hausbrandt.fragment.EditProfileActivity.GALLERY_PICTURE;
import static com.example.hausbrandt.fragment.EditProfileActivity.PERMISSION_CAMERA;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UploadRecieptFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UploadRecieptFragment extends Fragment implements ShopListAdapter.Onclick {

    ImageView editt, donee;
    EditText reciept_number;
    ImageView choose_image,notification;
    ImageView circleImageView;
    CountryCodePicker countryCodePicker;
    LinearLayout upload_reciept;

    PrefsHelper prefsHelper;
    TextView textView2;

    String image_count = "0", id;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    MultipartBody.Part image_file_multipart;

    Uri imageFilePath;
    File file;
    ImageView imageView2;
    ArrayList<ShopNearYouModel> shopNearYouModelList=new ArrayList<>();

    RecyclerView faavourite_recyclerView;
    LinearLayoutManager linearLayoutManager;
    ShopListAdapter shopListAdapter;

    String shop_id, shop_name,rating;


     CodeScannerView scanner_view;
    CodeScanner mCodeScanner;

    Context activity;

    String reciept_id = "";
    public UploadRecieptFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UploadRecieptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UploadRecieptFragment newInstance(String param1, String param2) {
        UploadRecieptFragment fragment = new UploadRecieptFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_upload_reciept, container, false);
        prefsHelper = new PrefsHelper(getContext());
        reciept_number = view.findViewById(R.id.reciept_number);
        choose_image = view.findViewById(R.id.choose_image);
        circleImageView = view.findViewById(R.id.imageView2);
        upload_reciept = view.findViewById(R.id.upload_reciept);
        textView2 = view.findViewById(R.id.textView2);
        scanner_view = view.findViewById(R.id.scanner_view);

        mCodeScanner = new CodeScanner(getContext(), scanner_view);

        this.activity = getContext();
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), "");
        image_file_multipart = MultipartBody.Part.createFormData("receipt", "", reqFile);


        faavourite_recyclerView = view.findViewById(R.id.shoprecycerview);
        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false);
        faavourite_recyclerView.setLayoutManager(linearLayoutManager);
        shopListAdapter = new ShopListAdapter(getContext(), shopNearYouModelList, this);
        faavourite_recyclerView.setAdapter(shopListAdapter);
        shopListAdapter.notifyDataSetChanged();

        getshops();




        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED ||ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CAMERA);
        } else {

         //   choose_pic_dialog(GALLERY_PICTURE, CAMERA_REQUEST);

        }

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), CustomerLevelActivity.class));
            }
        });
        upload_reciept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(shop_id!=null) {

                    if(!reciept_id.equals(""))
                    {
                        update(shop_id, reciept_id);
                    }
                    else
                    {
                        Toast.makeText(getContext(), R.string.pleasescan_qr, Toast.LENGTH_SHORT).show();

                    }

                }
                else
                {
                    Toast.makeText(getContext(), R.string.please_select_hop, Toast.LENGTH_SHORT).show();
                }
            }
        });

        notification = view.findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), NotificationActivity.class));

            }
        });

        choose_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED ||ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CAMERA);
                } else {

                    choose_pic_dialog(GALLERY_PICTURE, CAMERA_REQUEST);

                }
            }
        });


        reciept_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().equals("")) {
                    faavourite_recyclerView.setVisibility(View.GONE);

                } else {
                    faavourite_recyclerView.setVisibility(View.VISIBLE);
                    filter(editable.toString());
                }


            }
        });




        mCodeScanner.setCamera(CodeScanner.CAMERA_BACK); // or CAMERA_FRONT or specific camera id
        mCodeScanner.setFormats(CodeScanner.ALL_FORMATS); // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        mCodeScanner.setAutoFocusMode(AutoFocusMode.SAFE); // or CONTINUOUS
        mCodeScanner.setScanMode(ScanMode.SINGLE); // or CONTINUOUS or PREVIEW
        mCodeScanner.setAutoFocusEnabled(true); // Whether to enable auto focus or not
        mCodeScanner.setFlashEnabled(false);




        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      //  activityAttendaceScannerBinding.progressBarScanner.setVisibility(View.VISIBLE);

//                        if(type.equals("marks"))
//                        {


                           String scanner_data = result.getText();

                        reciept_id = scanner_data;


                        Toast.makeText(activity, scanner_data, Toast.LENGTH_SHORT).show();
                        Log.e("sdsdsd",scanner_data);
                            JSONObject jsonObject  = new JSONObject();
                            try {
                                jsonObject.put("scannedId",scanner_data);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        //    RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

                         //   attendanceScannerViewModel.scanForMarks(body);


//                            JSONObject jsonObject = new JSONObject();
//                            try {
//                                jsonObject.put("jobId",jobid);
//                                jsonObject.put("otp", result.getText());
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
//
//                            attendanceScannerViewModel.scanForMarks(body);



//                        }
//                        else
//                        {
//                            scanner_data = result.getText();
//                            JSONObject jsonObject = new JSONObject();
//                            try {
//                                jsonObject.put("jobId",jobid);
//                                jsonObject.put("otp", result.getText());
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
//
//                            attendanceScannerViewModel.send_attendace(body);
//                        }
                        //  Toast.makeText(AttendaceScannerActivity.this, result.getText(), Toast.LENGTH_SHORT).show();


                    }
                });
            }
        });
        scanner_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED ||ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CAMERA);
                } else {

                    mCodeScanner.startPreview();
                    //   choose_pic_dialog(GALLERY_PICTURE, CAMERA_REQUEST);

                }

            }
        });





        return view;
    }


    private void choose_pic_dialog(final int gallery, final int camera) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setTitle("Choose Image");
        builder1.setMessage("Select Image to Upload");
        builder1.setCancelable(true);


        builder1.setPositiveButton(
                "Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent pictureActionIntent = null;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(
                                pictureActionIntent,
                                gallery);
                    }
                });


        builder1.setNegativeButton(
                "Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //    Toast.makeText(activity, "Coming Soon", Toast.LENGTH_SHORT).show();


                        Intent pictureIntent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        if (pictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                            //Create a file to store the image
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File

                            }
                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(getContext(),
                                        "com.hausbrandt.provider", photoFile);
                                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                        photoURI);
                                startActivityForResult(pictureIntent,
                                        camera);
                            }
                        }

                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {
            CommonMethods.GetDialog(getContext(), "Please wait", "");
         //   Picasso.with(getContext()).load(imageFilePath).into(circleImageView);
            Log.e("Running", "Running1");
            Log.e("Running", imageFilePath.toString());
            Log.e("Running", "Running2");
            Log.e("Running", imageFilePath.toString());
            Log.e("Running", file.toString());
            Bitmap imageBitmap = null;
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageFilePath);
                circleImageView.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //  activityProfileBinding.addPicture.setImageBitmap(imageBitmap);
            try {
//
//                String mURI = imageFilePath.getEncodedPath();
////                        "file://".concat( new URI(imageFilePath.toString()).getPath());
//                File file = new File(mURI);
//                if (!file.exists()) {
//                    throw new Exception("File not found: ".concat(mURI));
//                }

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

//write the bytes in file
                FileOutputStream fo = new FileOutputStream(file);
                fo.write(bytes.toByteArray());

// remember close de FileOutput
                fo.close();

                RequestBody billFile = RequestBody.create(MediaType.parse("image/*"), file);
                image_file_multipart = MultipartBody.Part.createFormData("receipt", file.getName(), billFile);
                Log.e("Multipart", imageFilePath.toString());
                CommonMethods.dismissDialog();

                //   upload_image_by_retro_fit(image_file_multipart);

                //      profileViewModel.upload_doc(file);
                ///     upload_doc_init();

            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {

            CommonMethods.GetDialog(getContext(), "Please wait", "");
            if (data != null) {

                Uri selectedImage = data.getData();
                Log.e("Image", selectedImage.toString());
                Bitmap bitmap = null;
                Picasso.with(getContext()).load(selectedImage).into(circleImageView);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImage);
                    //   activityProfileBinding.addPicture.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
//                    file = new File("file://" + getPath(selectedImage));
//                    Log.e("Imagepath", "file://" + getPath(selectedImage));
                    //    file = new File(selectedImage.getPath());
                    //    String mURI = "file://".concat(getPath(selectedImage));
//                            "file://".concat( new URI(getPath(selectedImage)).getPath());


                    // Uri selectedImage = data.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContext().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.e("Running", picturePath);

                    // File file = new File("file://"+picturePath);
                    File file = new File(picturePath);
                    Log.e("Running", file.toString());

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

//write the bytes in file

                    FileOutputStream fo = new FileOutputStream(file);
                    fo.write(bytes.toByteArray());

// remember close de FileOutput
                    fo.close();
                    RequestBody billFile = RequestBody.create(MediaType.parse("image/*"), file);
                    image_file_multipart = MultipartBody.Part.createFormData("receipt", file.getName(), billFile);
                    Log.e("Multipart", image_file_multipart.toString());
                    CommonMethods.dismissDialog();

                    //     upload_image_by_retro_fit(image_file_multipart);
                    //  selectedImage.getPath();

                    //     profileViewModel.upload_doc(file);
                    //   upload_doc_init();
                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                }

            } else {
                Toast.makeText(getContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Cancelled",
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //  mCamera = getCameraInstance();
                }
                return;
            }
        }
    }


    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "edxpert" + timeStamp + "_";
        //    File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );


        RequestBody billFile = RequestBody.create(MediaType.parse("image/*"), image);
        image_file_multipart = MultipartBody.Part.createFormData("receipt", image.getName(), billFile);

        Log.e("Multipart1", image_file_multipart.toString());
        file = image;
        imageFilePath = Uri.fromFile(image);
        return image;
    }


    private void update(String shop_id, String reciept_id) {
        CommonMethods.GetDialog(getContext(), "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<LoginPojo> call3 = null;
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), shop_id);
        RequestBody reciept_idd = RequestBody.create(MediaType.parse("text/plain"), reciept_id);

        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.upload_reciept( name, reciept_idd, token);
        assert call3 != null;
        call3.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(getContext(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {

                                //  thankYouPopup();
                                    id =   response.body().getId();
                                Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                rate_coffee_popup();

                            } else {

                                Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }




    private void add_rating() {
        CommonMethods.GetDialog(getContext(), "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<LoginPojo> call3 = null;


        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.add_rating(id, image_count, token);
        assert call3 != null;
        call3.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(getContext(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {

                                //  thankYouPopup();
                                Toast.makeText(getContext(), R.string.coffe_rate_message, Toast.LENGTH_SHORT).show();

                                startActivity(new Intent(getContext(), CustomerLevelActivity.class));
                              //  rate_coffee_popup();

                            } else {

                                Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }


    private void getshops() {

        CommonMethods.GetDialog(getActivity(),"Please wait","");
       ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<ShopNearYouPojo> call3 = null;
        call3 = apiInterface.nearmeshopaddress("mo",Constants.lat,Constants.lang,"Bearer " + prefsHelper.getPref(Constants.authToken));
        // call3 = apiInterface.nearmeshopaddress("mo",mLastLocation.getLatitude(),mLastLocation.getLongitude(),"Bearer " + prefsHelper.getPref(Constants.authToken));
        assert call3 != null;
        call3.enqueue(new Callback<ShopNearYouPojo>() {
            @Override
            public void onResponse(Call<ShopNearYouPojo> call, retrofit2.Response<ShopNearYouPojo> response) {
                try {
                    if(response.isSuccessful()){
                        if (response.body() == null) {
                            Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            shopNearYouModelList=response.body().getData();
                            Log.d("RESs", "" + shopNearYouModelList.size());
                            if(shopNearYouModelList.size()>0){
                               // shopListAdapter = new ShopListAdapter(getContext(), shopNearYouModelList, this);
                                shopListAdapter.updateList(shopNearYouModelList);
                                faavourite_recyclerView.setAdapter(shopListAdapter);
                                shopListAdapter.notifyDataSetChanged();

                            }


                        }

                    }else {
                        Toast.makeText(getActivity(), ""+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception ex){
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<ShopNearYouPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    public void refer_popup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refer_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button done_btn=dialog.findViewById(R.id.done_btn);
        TextView textView1=dialog.findViewById(R.id.text1);
        TextView textView2=dialog.findViewById(R.id.text2);
        textView1.setText(R.string.your_have_success_fully);
        textView2.setText(R.string.you_);
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                reciept_number.setText("");
                circleImageView.setImageResource(0);

                dialog.dismiss();
            }
        });
    }


    public void rate_coffee_popup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rate_cofee_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button done_btn=dialog.findViewById(R.id.done_btn);
        final ImageView imageView1=dialog.findViewById(R.id.coffee1);
       final ImageView imageView2=dialog.findViewById(R.id.coffee2);
       final ImageView imageView3=dialog.findViewById(R.id.coffee3);
       final ImageView imageView4=dialog.findViewById(R.id.coffee4);
       final ImageView imageView5=dialog.findViewById(R.id.coffee5);
       final TextView skip =dialog.findViewById(R.id.skip);


       skip.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               dialog.dismiss();
           }
       });
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                reciept_number.setText("");
                circleImageView.setImageResource(0);

                add_rating();

                dialog.dismiss();
            }
        });

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean_grey);
                imageView3.setImageResource(R.drawable.coffee_bean_grey);
                imageView4.setImageResource(R.drawable.coffee_bean_grey);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "1";
            }
        });


        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean_grey);
                imageView4.setImageResource(R.drawable.coffee_bean_grey);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "2";
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean);
                imageView4.setImageResource(R.drawable.coffee_bean_grey);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "3";
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean);
                imageView4.setImageResource(R.drawable.coffee_bean);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "4";
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean);
                imageView4.setImageResource(R.drawable.coffee_bean);
                imageView5.setImageResource(R.drawable.coffee_bean);
                image_count = "5";
            }
        });
    }


    @Override
    public void clickitem(String id, String name) {

        shop_id = id;
        shop_name = name;
        reciept_number.setText(shop_name);
        faavourite_recyclerView.setVisibility(View.GONE);
    }

    void filter(String text){
        ArrayList<ShopNearYouModel> temp = new ArrayList();
        for(ShopNearYouModel d: shopNearYouModelList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getShopTitle().toLowerCase().contains(text)){
                temp.add(d);
            }
        }
        //update recyclerview
        shopListAdapter.updateList(temp);
    }
}