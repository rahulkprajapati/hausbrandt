package com.example.hausbrandt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hausbrandt.R;
import com.example.hausbrandt.pojo.NotificationData;

import java.util.ArrayList;

public class NoticationAdapter extends RecyclerView.Adapter<NoticationAdapter.ViewHolder> {


    Context context;
    ArrayList<NotificationData> stringArrayList;
    public NoticationAdapter(Context context, ArrayList<NotificationData> stringArrayList) {
        this.context =context;
    this.stringArrayList = stringArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_recyclerview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoticationAdapter.ViewHolder holder, int position) {

        holder.message.setText(stringArrayList.get(position).getMessage());
        holder.time.setText(stringArrayList.get(position).getDate());
    }



    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView message,time;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message);
            time = itemView.findViewById(R.id.time);
        }
    }
}
