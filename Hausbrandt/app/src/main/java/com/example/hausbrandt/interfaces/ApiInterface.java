package com.example.hausbrandt.interfaces;

import com.example.hausbrandt.pojo.CustomerLevelResponse;
import com.example.hausbrandt.pojo.ForgotPojo;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.pojo.NotificationData;
import com.example.hausbrandt.pojo.NotificationResponse;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.pojo.ShopNearYouPojo;
import com.example.hausbrandt.pojo.aboutModel.AboutResponse;
import com.example.hausbrandt.pojo.faq.FaqResponse;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteResponse;
import com.example.hausbrandt.utils.Constants;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

//    @GET("sliderimage.php")
//    Call<Sliderimage> getsliderimage();
//
//
//    // Register
    @FormUrlEncoded
    @POST(Constants.REGISTER)
    Call<RegisterPojo>register(@Field("name") String name, @Field("surname") String surname
            ,@Field("username") String username,@Field("email") String email,
            @Field("mobile_number") String mobile_number,@Field("password") String password,@Field("latitude")
            double latitude,@Field("longitude") double longitude,@Field("deviceType") Integer deviceType,@Field("deviceId") String deviceId,@Field("phone_code") String phone_code,@Field("referCode") String referCode );
    //forgot password
    @FormUrlEncoded
    @POST(Constants.forgotpassword_send_request)
    Call<ForgotPojo>forgotpassword_send_request(@Field("userId") String userid);

    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.report)
    Call<ForgotPojo>report_send(@Field("report") String report,@Header("Authorization") String authHeader);


    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.help)
    Call<ForgotPojo>help(@Field("message") String report,@Header("Authorization") String authHeader);


    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.feedback)
    Call<ForgotPojo>feedback_send(@Field("rating") String rating,@Field("feedback") String feedback,@Header("Authorization") String authHeader);


    //otp verify
    @FormUrlEncoded
    @POST(Constants.forgotpassword_otp_verify)
    Call<RegisterPojo>forgotpassword_otp_verify(@Field("tempId") String tempId,@Field("otp") String otp);

 //login
    @FormUrlEncoded
    @POST(Constants.login)
    Call<LoginPojo>login(@Field("userId") String tempId, @Field("password") String password,@Field("latitude") double latitude,@Field("longitude") double longitude);

    //otp verify
    @FormUrlEncoded
    @POST(Constants.reset_password)
    Call<RegisterPojo>reset_password(@Field("tempId") String tempId,@Field("password") String password,@Field("password_confirmation") String password_confirmation);

    // social login
    @FormUrlEncoded
    @POST(Constants.sociallogin)
    Call<LoginPojo>sociallogin(@Field("name") String name, @Field("surname") String surname
            ,@Field("userId") String username,@Field("latitude")
                                       double latitude,@Field("longitude") double longitude,@Field("deviceType") Integer deviceType,@Field("deviceId") String deviceId);

    // nearbyshop
    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.nearmeshopaddress)
    Call<ShopNearYouPojo>nearmeshopaddress(@Field("lang") String lang, @Field("latitude") double latitude, @Field("longitude") double longitude, @Header("Authorization") String authHeader);

    // nearbyshop
    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.addWishList)
    Call<RegisterPojo>addfourite(@Field("shop_id") int shop_id, @Header("Authorization") String authHeader);



    @Multipart
    @Headers("Accept:application/json")
    @POST(Constants.update_profile)
    Call<LoginPojo> upload_profile(@Part MultipartBody.Part profile_pic,
                                   @Part("name") RequestBody name,
                                   @Part("surname") RequestBody surname,
                                   @Part("username") RequestBody username,
                                   @Part("email") RequestBody email,
                                   @Part("mobile_number") RequestBody mobile_number,
                                   @Part("password") RequestBody password,
                                   @Part("phone_code") RequestBody phone_code,
                                   @Header("authorization") String authorization);

    @Multipart
    @Headers("Accept:application/json")
    @POST(Constants.receiptAdd)
    Call<LoginPojo> upload_reciept(
                                   @Part("coffee_shop_id") RequestBody receipt_no,
                                   @Part("qrCodeResponse") RequestBody qrCodeResponse,
                                   @Header("authorization") String authorization);

    

//    @Part MultipartBody.Part profile_pic,
//    @FormUrlEncoded
//    @Headers("Accept:application/json")
//    @POST(Constants.receiptAdd)
//    Call<LoginPojo> upload_reciept(
//                                   @Field("coffee_shop_id") String receipt_no,
//                                   @Field("qrCodeResponse") String qrCodeResponse,
//                                   @Field("authorization") String authorization);
//


    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.page_data)
    Call<AboutResponse>get_about(@Field("lang") String lang,
                                 @Field("pageId") String page_id,
                                 @Header("Authorization") String authHeader);


    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.get_favourite)
    Call<FavouriteResponse>get_fav(@Field("lang") String lang,
                                     @Header("Authorization") String authHeader);


    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.customer_level)
    Call<CustomerLevelResponse>get_level(@Field("lang") String lang,
                                         @Header("Authorization") String authHeader);



    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.earnByRefer)
    Call<LoginPojo>send_refercode(@Field("userId") String userId,
                                         @Header("Authorization") String authHeader);



   @FormUrlEncoded
   @Headers("Accept:application/json")
   @POST(Constants.addRating)
   Call<LoginPojo>add_rating(@Field("id") String id,
                             @Field("rating") String rating,
                                 @Header("Authorization") String authHeader);




   @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST(Constants.faq)
    Call<FaqResponse>get_faq(@Field("lang") String lang,
                             @Header("Authorization") String authHeader);


    @Headers("Accept:application/json")
    @GET(Constants.notification)
    Call<NotificationResponse>get_notification(@Header("Authorization") String authHeader);


}
