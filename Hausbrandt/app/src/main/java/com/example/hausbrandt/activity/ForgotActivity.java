package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.ForgotPojo;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView back;
    private ApiInterface apiInterface;
    private EditText username;
    private String str_userid;
    private Button submit_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        username = findViewById(R.id.username);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }

    private void getText(){
        str_userid=username.getText().toString();
    }

    private void  Submit() {
        CommonMethods.GetDialog(ForgotActivity.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<ForgotPojo> call3 = null;
        call3 = apiInterface.forgotpassword_send_request(str_userid);
        assert call3 != null;
        call3.enqueue(new Callback<ForgotPojo>() {
            @Override
            public void onResponse(Call<ForgotPojo> call, retrofit2.Response<ForgotPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(ForgotActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();
                                startActivity(new Intent(ForgotActivity.this,OtpActivity.class).putExtra("temp_id",response.body().getTempId()));
                                Toast.makeText(ForgotActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            } else {

                                Toast.makeText(ForgotActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(ForgotActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<ForgotPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_btn:
                getText();
                if(str_userid.equalsIgnoreCase("")){
                    username.setError("Please enter required field");
                }else {
                    Submit();
                }

                break;
        }
    }
}