package com.example.hausbrandt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.example.hausbrandt.activity.DashBoardActivity;
import com.example.hausbrandt.activity.LoginActivity;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;
import com.google.android.gms.maps.model.Dash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashScreen extends AppCompatActivity {


    PrefsHelper prefsHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        prefsHelper = new PrefsHelper(this);
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.hausbrandt", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(prefsHelper.getPref(Constants.authToken)!=null)
                {
                    startActivity(new Intent(SplashScreen.this, DashBoardActivity.class));
                    finish();
                }
                else
                {
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                    finish();
                }


                }



        },3000);

    }
}