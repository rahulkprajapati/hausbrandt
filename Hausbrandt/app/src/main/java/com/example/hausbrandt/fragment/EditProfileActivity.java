package com.example.hausbrandt.fragment;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.activity.LoginActivity;
import com.example.hausbrandt.activity.SignUpActivity;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;
import com.hbb20.CountryCodePicker;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class EditProfileActivity extends Fragment {


    ImageView editt, donee;
    EditText name_profile, surname_profile, username_profile, email_profile, phone_profile;
    LinearLayout choose_image;
    ImageView circleImageView;
    CountryCodePicker countryCodePicker;
    String country_code;

    PrefsHelper prefsHelper;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static final int GALLERY_PICTURE = 130;
    public static final int CAMERA_REQUEST = 131;
    public static final int PERMISSION_CAMERA = 231;

    private String mParam1;
    private String mParam2;

    MultipartBody.Part image_file_multipart;

    Uri imageFilePath;
    File file;

    ProgressBar progresss_bar;
    public EditProfileActivity() {
        // Required empty public constructor
    }


    public static EditProfileActivity newInstance(String param1, String param2) {
        EditProfileActivity fragment = new EditProfileActivity();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_edit_profile, container, false);


        prefsHelper = new PrefsHelper(getContext());
        editt = view.findViewById(R.id.editt);
        donee = view.findViewById(R.id.donee);
        name_profile = view.findViewById(R.id.name_profile);
        surname_profile = view.findViewById(R.id.surname_profile);
        username_profile = view.findViewById(R.id.username_profile);
        email_profile = view.findViewById(R.id.email_profile);
        phone_profile = view.findViewById(R.id.phone_profile);
        choose_image = view.findViewById(R.id.choose_image);
        circleImageView = view.findViewById(R.id.circleImageView);
        countryCodePicker = view.findViewById(R.id.country_code);
        progresss_bar = view.findViewById(R.id.progresss_bar);



        name_profile.setFocusable(false);
        surname_profile.setFocusable(false);
        choose_image.setClickable(false);
        choose_image.setEnabled(false);
        choose_image.setFocusable(false);
        choose_image.setFocusableInTouchMode(false);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        name_profile.setText( (String) prefsHelper.getPref(Constants.name));
                surname_profile.setText( (String) prefsHelper.getPref(Constants.surname));
                email_profile.setText( (String) prefsHelper.getPref(Constants.email_id));

        if(prefsHelper.getPref(Constants.profile)!=null && !prefsHelper.getPref(Constants.profile).equals(""))
        {
            Picasso.with(getContext()).load(String.valueOf(prefsHelper.getPref(Constants.profile))).into(circleImageView);
        }


        if(prefsHelper.getPref(Constants.mobile)!=null)
        {
            countryCodePicker.setCountryForNameCode((String) prefsHelper.getPref(Constants.countrycode));
            if(String.valueOf(prefsHelper.getPref(Constants.mobile)).contains("00005"))
            {
                phone_profile.setText("");

            }
            else
            {

                if(!String.valueOf(prefsHelper.getPref(Constants.mobile)).equals("")) {
                    phone_profile.setText((String) prefsHelper.getPref(Constants.mobile));
                }
                else
                {
                    phone_profile.setText("");

                }
            }


            if(String.valueOf(prefsHelper.getPref(Constants.username)).contains("user_"))
            {
                username_profile.setText("");

            }
            else
            {
                if(!String.valueOf(prefsHelper.getPref(Constants.username)).equals("")) {
                    username_profile.setText((String) prefsHelper.getPref(Constants.username));
                }
                else
                {
                    username_profile.setText("");

                }


            }




        }



                country_code = countryCodePicker.getSelectedCountryCode();
        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                country_code = countryCodePicker.getSelectedCountryCode();
                Log.d("country_code", country_code);
            }
        });


        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), "");
        image_file_multipart = MultipartBody.Part.createFormData("profile_pic", "", reqFile);


        editt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name_profile.setFocusable(true);
                name_profile.setFocusableInTouchMode(true);
                surname_profile.setFocusable(true);
                surname_profile.setFocusableInTouchMode(true);
                phone_profile.setFocusable(true);
                phone_profile.setFocusableInTouchMode(true);
                email_profile.setFocusable(true);
                email_profile.setFocusableInTouchMode(true);
                username_profile.setFocusable(true);
                username_profile.setFocusableInTouchMode(true);
                choose_image.setClickable(true);
                choose_image.setFocusable(true);
                choose_image.setFocusableInTouchMode(true);
                choose_image.setEnabled(true);



                //  username_profile.setFocusable(true);
                //  email_profile.setFocusable(true);
                // phone_profile.setFocusable(true);
                donee.setVisibility(View.VISIBLE);
                editt.setVisibility(View.GONE);

            }
        });


        donee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                update();
            }
        });

        choose_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED ||ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CAMERA);

                } else {

                    choose_pic_dialog(GALLERY_PICTURE, CAMERA_REQUEST);

                }
            }
        });


        return view;
    }


    private void choose_pic_dialog(final int gallery, final int camera) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setTitle("Choose Image");
        builder1.setMessage("Select Image to Upload");
        builder1.setCancelable(true);


        builder1.setPositiveButton(
                "Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent pictureActionIntent = null;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(
                                pictureActionIntent,
                                gallery);
                    }
                });


        builder1.setNegativeButton(
                "Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //    Toast.makeText(activity, "Coming Soon", Toast.LENGTH_SHORT).show();


                        Intent pictureIntent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        if (pictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                            //Create a file to store the image
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File

                            }
                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(getContext(),
                                        "com.hausbrandt.provider", photoFile);
                                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                        photoURI);
                                startActivityForResult(pictureIntent,
                                        camera);
                            }
                        }

                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        progresss_bar.setVisibility(View.VISIBLE);

        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {


            CommonMethods.GetDialog(getContext(), "Please wait", "");
            Picasso.with(getContext()).load(imageFilePath).into(circleImageView);
            Log.e("Running", "Running1");
            Log.e("Running", imageFilePath.toString());
            Log.e("Running", "Running2");
            Log.e("Running", imageFilePath.toString());
            Log.e("Running", file.toString());
            Bitmap imageBitmap = null;
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //  activityProfileBinding.addPicture.setImageBitmap(imageBitmap);
            try {
//
//                String mURI = imageFilePath.getEncodedPath();
////                        "file://".concat( new URI(imageFilePath.toString()).getPath());
//                File file = new File(mURI);
//                if (!file.exists()) {
//                    throw new Exception("File not found: ".concat(mURI));
//                }

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

//write the bytes in file
                FileOutputStream fo = new FileOutputStream(file);
                fo.write(bytes.toByteArray());

// remember close de FileOutput
                fo.close();

                RequestBody billFile = RequestBody.create(MediaType.parse("image/*"), file);
                image_file_multipart = MultipartBody.Part.createFormData("profile_pic", file.getName(), billFile);
                Log.e("Multipart", imageFilePath.toString());
                CommonMethods.dismissDialog();

                //   upload_image_by_retro_fit(image_file_multipart);

                //      profileViewModel.upload_doc(file);
                ///     upload_doc_init();
                progresss_bar.setVisibility(View.GONE);

            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {

            CommonMethods.GetDialog(getContext(), "Please wait", "");
            if (data != null) {

                Uri selectedImage = data.getData();
                Log.e("Image", selectedImage.toString());
                Bitmap bitmap = null;
                Picasso.with(getContext()).load(selectedImage).into(circleImageView);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImage);
                    //   activityProfileBinding.addPicture.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
//                    file = new File("file://" + getPath(selectedImage));
//                    Log.e("Imagepath", "file://" + getPath(selectedImage));
                    //    file = new File(selectedImage.getPath());
                    //    String mURI = "file://".concat(getPath(selectedImage));
//                            "file://".concat( new URI(getPath(selectedImage)).getPath());


                    // Uri selectedImage = data.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContext().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.e("Running", picturePath);

                    // File file = new File("file://"+picturePath);
                    File file = new File(picturePath);
                    Log.e("Running", file.toString());

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

//write the bytes in file

                    FileOutputStream fo = new FileOutputStream(file);
                    fo.write(bytes.toByteArray());

// remember close de FileOutput
                    fo.close();
                    RequestBody billFile = RequestBody.create(MediaType.parse("image/*"), file);
                    image_file_multipart = MultipartBody.Part.createFormData("profile_pic", file.getName(), billFile);
                    Log.e("Multipart", image_file_multipart.toString());
                    CommonMethods.dismissDialog();

                    progresss_bar.setVisibility(View.GONE);

                    //     upload_image_by_retro_fit(image_file_multipart);
                    //  selectedImage.getPath();

                    //     profileViewModel.upload_doc(file);
                    //   upload_doc_init();
                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                }

            } else {
                Toast.makeText(getContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Cancelled",
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //  mCamera = getCameraInstance();
                }
                return;
            }
        }
    }


    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "edxpert" + timeStamp + "_";
        //    File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );


        RequestBody billFile = RequestBody.create(MediaType.parse("image/*"), image);
        image_file_multipart = MultipartBody.Part.createFormData("edxpert", image.getName(), billFile);

        Log.e("Multipart1", image_file_multipart.toString());
        file = image;
        imageFilePath = Uri.fromFile(image);
        return image;
    }


    private void update() {
        CommonMethods.GetDialog(getContext(), "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<LoginPojo> call3 = null;
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), name_profile.getText().toString());
        RequestBody surname = RequestBody.create(MediaType.parse("text/plain"), surname_profile.getText().toString());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), email_profile.getText().toString());
        RequestBody phoneprofile = RequestBody.create(MediaType.parse("text/plain"), phone_profile.getText().toString());
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"), username_profile.getText().toString());
        RequestBody password = RequestBody.create(MediaType.parse("text/plain"), (String) prefsHelper.getPref(Constants.password));
        RequestBody county = RequestBody.create(MediaType.parse("text/plain"), country_code);
     //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.upload_profile(image_file_multipart, name, surname, username, email, phoneprofile, password, county, token);
        assert call3 != null;
        call3.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(getContext(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();
                                Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                name_profile.setFocusable(false);
                                surname_profile.setFocusable(false);
                                username_profile.setFocusable(false);
                                email_profile.setFocusable(false);
                                phone_profile.setFocusable(false);
                                phone_profile.setFocusableInTouchMode(false);
                                username_profile.setFocusable(false);
                                username_profile.setFocusableInTouchMode(false);
                                email_profile.setFocusable(false);
                                email_profile.setFocusableInTouchMode(false);
                                donee.setVisibility(View.GONE);
                                editt.setVisibility(View.VISIBLE);


                                choose_image.setEnabled(false);

                            //    prefsHelper.savePref(Constants.authToken, response.body().getToken());
                                prefsHelper.savePref(Constants.name, response.body().getName());
                                prefsHelper.savePref(Constants.surname, response.body().getSurname());
                                prefsHelper.savePref(Constants.mobile, response.body().getMobileNumber());
                                prefsHelper.savePref(Constants.username, response.body().getUsername());

                                if(response.body().getProfilePic()!=null && !response.body().getProfilePic().equals("")) {
                                    prefsHelper.savePref(Constants.profile, response.body().getProfilePic());
                                }
                           //     prefsHelper.savePref(Constants.username, response.body().getUsername());
                           //     prefsHelper.savePref(Constants.email_id, response.body().getEmail());
                           //     prefsHelper.savePref(Constants.mobile, response.body().getMobileNumber());

                            } else {

                                Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }


}