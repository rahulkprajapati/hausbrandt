package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.ForgotPojo;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.hausbrandt.R.string.please_enter_otp;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {

    private String temp_id,str_otp;
    private ImageView back;
    private ApiInterface apiInterface;
    private PinView pin;
    private Button submit_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        back=findViewById(R.id.back);
        pin=findViewById(R.id.otp_view);
        submit_btn=findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
        back.setOnClickListener(this);
        temp_id=getIntent().getStringExtra("temp_id");
    }

    private void getText(){
        str_otp=pin.getText().toString();
    }

    private void  Submit() {
        CommonMethods.GetDialog(OtpActivity.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RegisterPojo> call3 = null;
        call3 = apiInterface.forgotpassword_otp_verify(temp_id,str_otp);
        assert call3 != null;
        call3.enqueue(new Callback<RegisterPojo>() {
            @Override
            public void onResponse(Call<RegisterPojo> call, retrofit2.Response<RegisterPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(OtpActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();
                                startActivity(new Intent(OtpActivity.this,PasswordReset.class).putExtra("temp_id",temp_id));
                                Toast.makeText(OtpActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();

                            } else {

                                Toast.makeText(OtpActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(OtpActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<RegisterPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_btn:
                getText();
                if(str_otp.equalsIgnoreCase("")){
                    Toast.makeText(this, please_enter_otp, Toast.LENGTH_SHORT).show();
                }else {
                    Submit();
                }

                break;
            case R.id.back:
                finish();
                break;
        }
    }
}