package com.example.hausbrandt.utils;

import android.content.Context;
import android.text.Editable;


public class Constants {


    //live url
    public static final String BASE_URL_REGISTRATION = "https://promotebio.com/housbrandt/api/";
    public static final String REGISTER = "userRegistration";
    public static final String forgotpassword_send_request = "forgotpassword_send_request";
    public static final String report = "report";
    public static final String help = "help";
    public static final String feedback = "feedback";
    public static final String forgotpassword_otp_verify = "forgotpassword_otp_verify";
    public static final String reset_password = "reset_password";
    public static final String login = "login";
    public static final String sociallogin = "social-login";
    public static final String update_profile = "updateProfile";
    public static final String receiptAdd = "receiptAdd";
    public static final String page_data = "pageData";
    public static final String get_favourite = "getWishList";
    public static final String customer_level = "beans-treasure";
    public static final String earnByRefer = "earnByRefer";
    public static final String addRating = "addRating";
    public static final String faq = "faq";
    public static final String notification = "notification";
    public static final String nearmeshopaddress = "near-me-shop-address";
    public static final String addWishList = "addWishList";
  //  public static final String CLIENT_ID = "318306456062534";
    public static final String CLIENT_ID = "2853173254952279";
    public static final String CLIENT_SECRET = "acfeb19e0b093430da93412042112293";
  //  public static final String CLIENT_SECRET = "56714838b522d30c1d625d6e872a9b86";
    public static final String CALLBACK_URL = "https://socialsizzle.heroku.com/auth/";

    public static final String authToken = "authToken";
    public static final String name = "name";
    public static final String surname = "surname";
    public static final String username = "username";
    public static final String password = "password";
    public static final String email_id = "email_id";
    public static final String profile = "profile";
    public static final String mobile = "mobile";
    public static final String countrycode = "countrycode";
    public static final String language = "language";
    public static double lat = 28.788;
    public static double lang = 77.888;

}