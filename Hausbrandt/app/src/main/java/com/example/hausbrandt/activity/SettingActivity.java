package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.LocaleHelper;
import com.example.hausbrandt.utils.PrefsHelper;

public class SettingActivity extends AppCompatActivity {

    RadioButton english, montegg;
    ImageView languge_click;
    LinearLayout language_layout,report_problem,leave_feedback,rules_of_game,privacy_policy;

    PrefsHelper prefsHelper;

    boolean click = false;


    ImageView back,notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        back  = findViewById(R.id.back);
        english  = findViewById(R.id.english);
        montegg  = findViewById(R.id.montegg);
        languge_click  = findViewById(R.id.langauge_click);
        language_layout  = findViewById(R.id.language_layout);
        report_problem  = findViewById(R.id.report_problem);
        leave_feedback  = findViewById(R.id.leave_feedback);
        rules_of_game  = findViewById(R.id.rules_of_game);
        privacy_policy  = findViewById(R.id.privacy_policy);

        notification = findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SettingActivity.this, NotificationActivity.class));

            }
        });


        privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(SettingActivity.this, PrivacyPolicyActivity.class)
                        .putExtra("code","6"));
            }
        });


        rules_of_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SettingActivity.this, PrivacyPolicyActivity.class)
                .putExtra("code","9"));

            }
        });        report_problem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SettingActivity.this, ReportProblem.class));

            }
        });
        leave_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SettingActivity.this, Feedback.class));

            }
        });

        prefsHelper = new PrefsHelper(this);



        if(prefsHelper.getPref(Constants.language,null)!=null)

        {
            if(prefsHelper.getPref(Constants.language,null).equals("en")) {
                english.setChecked(true);
                montegg.setChecked(false);
            }
            else
            {
                english.setChecked(false);
                montegg.setChecked(true);
            }

        }
        else
        {
            english.setChecked(true);
            montegg.setChecked(false);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                english.setChecked(true);
                montegg.setChecked(false);
                LocaleHelper.setLocale(SettingActivity.this, "en");
                prefsHelper.savePref(Constants.language,"en");

                startActivity(new Intent(SettingActivity.this, DashBoardActivity.class));
                finish();
            }
        });


        montegg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                english.setChecked(false);
                montegg.setChecked(true);
                LocaleHelper.setLocale(SettingActivity.this, "hi");
                prefsHelper.savePref(Constants.language,"mo");
                startActivity(new Intent(SettingActivity.this, DashBoardActivity.class));
                finish();
            }
        });


        languge_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(!click)
                {
                    languge_click.setRotation(90);
                    language_layout.setVisibility(View.VISIBLE);
                    click = true;
                }
                else
                {
                    language_layout.setVisibility(View.GONE);
                    click = false;
                }



            }
        });
    }

    private void setdata()
    {

    }

}