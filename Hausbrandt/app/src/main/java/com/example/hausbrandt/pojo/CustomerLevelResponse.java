
package com.example.hausbrandt.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerLevelResponse {

    @SerializedName("your_level")
    @Expose
    private String yourLevel;
    @SerializedName("week")
    @Expose
    private Integer week;
    @SerializedName("month")
    @Expose
    private Integer month;
    @SerializedName("year")
    @Expose
    private Integer year;

    public String getYourLevel() {
        return yourLevel;
    }

    public void setYourLevel(String yourLevel) {
        this.yourLevel = yourLevel;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

}
