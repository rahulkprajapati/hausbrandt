package com.example.hausbrandt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hausbrandt.R;
import com.example.hausbrandt.model.ShopNearYouModel;
import com.example.hausbrandt.pojo.ShopNearYouPojo;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteList;

import java.util.ArrayList;

import retrofit2.Callback;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {


    Context context;
    Onclick onclick;
    ArrayList<ShopNearYouModel> stringArrayList;
    public ShopListAdapter(Context context, ArrayList<ShopNearYouModel> stringArrayList, Onclick onclick) {
        this.context =context;
        this.onclick = onclick;
        this.stringArrayList = stringArrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.shop_list_recyclerview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopListAdapter.ViewHolder holder, final int position) {


        holder.shop_name.setText(stringArrayList.get(position).getShopTitle());
        holder.abut_shop.setText(stringArrayList.get(position).getAddress());
        holder.item_lienar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onclick.clickitem(String.valueOf(stringArrayList.get(position).getId()), stringArrayList.get(position).getShopTitle());
            }
        });




    }



    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout item_lienar;

        TextView time,abut_shop,shop_name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            shop_name = itemView.findViewById(R.id.shop_name);
            abut_shop = itemView.findViewById(R.id.abut_shop);
            item_lienar = itemView.findViewById(R.id.item_lienar);

        }
    }

    public interface Onclick
    {

        void clickitem(String id , String name);
    }

    public void updateList(ArrayList<ShopNearYouModel> list){
        this.stringArrayList = list;
        notifyDataSetChanged();

    }

}
