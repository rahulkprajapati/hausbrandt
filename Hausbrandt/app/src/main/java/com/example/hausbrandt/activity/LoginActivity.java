package com.example.hausbrandt.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.MainActivity;
import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.GPSTracker;
import com.example.hausbrandt.utils.PrefsHelper;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{


    private static final int PERMISSION_READ_PHONE_STATE = 100;
    TextView logn_social, signup,forgot_password;
    Button login;
    private ApiInterface apiInterface;
    private EditText username,password;
    private String str_password,str_username;
    GPSTracker gpsTracker;
    PrefsHelper prefsHelper;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        logn_social = findViewById(R.id.loginn_social_media);
        signup = findViewById(R.id.signup);
        login = findViewById(R.id.login);
        gpsTracker=new GPSTracker(LoginActivity.this);
        login.setOnClickListener(this);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        prefsHelper = new PrefsHelper(LoginActivity.this);
        forgot_password = findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(LoginActivity.this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_READ_PHONE_STATE);
        } else {
            // goToNext();
        }
        logn_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this, SocialMediaLogin.class));
            }
        });





        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));


            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.forgot_password:
                startActivity(new Intent(LoginActivity.this, ForgotActivity.class));
                break;
            case R.id.login:
               getText();
                if(str_username.equalsIgnoreCase("")){
                   username.setError("Please enter username");
               }
                else if(str_password.equalsIgnoreCase("")){
                    password.setError("Please enter password.");
                }else {
                   Submit();
               }
              // Submit();
                break;

        }
    }

    private void getText(){
        str_password=password.getText().toString();
        str_username=username.getText().toString();
    }

    private void  Submit() {
        CommonMethods.GetDialog(LoginActivity.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginPojo> call3 = null;
        call3 = apiInterface.login(str_username,str_password,gpsTracker.getLatitude(),gpsTracker.getLongitude());
        assert call3 != null;
        call3.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(LoginActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();
                                prefsHelper.savePref(Constants.authToken,response.body().getToken());
                                prefsHelper.savePref(Constants.name,response.body().getName());
                                prefsHelper.savePref(Constants.surname,response.body().getSurname());
                                prefsHelper.savePref(Constants.username,response.body().getUsername());
                                prefsHelper.savePref(Constants.email_id,response.body().getEmail());
                                prefsHelper.savePref(Constants.password,str_password);
                                prefsHelper.savePref(Constants.mobile,response.body().getMobileNumber());
                                prefsHelper.savePref(Constants.countrycode,response.body().getPhone_code());
                                startActivity(new Intent(LoginActivity.this,DashBoardActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                Toast.makeText(LoginActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            //    finish();

                            } else {

                                Toast.makeText(LoginActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(LoginActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
}