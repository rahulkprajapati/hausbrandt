package com.example.hausbrandt.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hausbrandt.R;
import com.example.hausbrandt.pojo.faq.FaqDatum;
import com.example.hausbrandt.utils.ExpandableCardLayout;

import java.util.List;

public class FaqAdapterSecond extends  RecyclerView.Adapter<FaqAdapterSecond.ViewHolder> {
    private Context context;
    private int index  ;
    private RecyclerView recyclerView;
    private List<FaqDatum> faqContentList;
    public FaqAdapterSecond(Context context, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_faqs_second,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (index == position) {
            holder.expandable.expand(false);
        } else {
            holder.expandable.collapse(false);
        }
        holder.title.setText(faqContentList.get(position).getQuestion());
        holder.detail.setText(Html.fromHtml(faqContentList.get(position).getAnswer()));
    }

    @Override
    public int getItemCount() {
        return faqContentList == null ? 0 : faqContentList.size();
    }
    public void setDataFaqList(List<FaqDatum> faqContentList) {
        this.faqContentList = faqContentList;
        notifyDataSetChanged();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView downArrow;
        private ExpandableCardLayout expandable;
        private TextView detail;
        LinearLayout main_linear;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            expandable = (ExpandableCardLayout) itemView.findViewById(R.id.ell);
            detail = (TextView) itemView.findViewById(R.id.detail);
            downArrow = (ImageView) itemView.findViewById(R.id.downArrow);
            main_linear =  itemView.findViewById(R.id.main_linear);
            expandable.setOnExpansionUpdateListener(new ExpandableCardLayout.OnExpansionUpdateListener() {
                @Override
                public void onExpansionUpdate(float expansionFraction) {
                    downArrow.setRotation(180 * expansionFraction);
                }
            });
            main_linear.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(index);
            if (holder != null) {
                holder.expandable.collapse();
            }
            index = getLayoutPosition();
            new ExpansionEvent(!expandable.isExpanded());
            expandable.toggle();
        }

    }

    public class ExpansionEvent {
        public boolean isExpanded;

        public ExpansionEvent(boolean isExpanded) {
            this.isExpanded = isExpanded;
        }
    }

}
