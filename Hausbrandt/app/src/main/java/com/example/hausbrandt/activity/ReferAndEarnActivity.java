package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.CustomerLevelResponse;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.hausbrandt.R.string.please_enter_email_address;

public class ReferAndEarnActivity extends AppCompatActivity {




    PrefsHelper prefsHelper;
    EditText email_refer;
    Button upload_refeer_code;
    ImageView back_button,notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_and_earn);

        prefsHelper = new PrefsHelper(this);
        email_refer = findViewById(R.id.email_refer);
        upload_refeer_code = findViewById(R.id.upload_refeer_code);
        back_button = findViewById(R.id.back_button);


        notification = findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ReferAndEarnActivity.this, NotificationActivity.class));

            }
        });
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        upload_refeer_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = email_refer.getText().toString();
                if(email_refer.getText().toString().equals(""))
                {
                    Toast.makeText(ReferAndEarnActivity.this, please_enter_email_address, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    send_refer_code(email);
                }


            }
        });

    }


    private void send_refer_code(String email_id) {
        CommonMethods.GetDialog(this, "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<LoginPojo> call3 = null;
        //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.send_refercode(email_id,  token);
        assert call3 != null;
        call3.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {

                            Toast.makeText(ReferAndEarnActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if(response.body().getStatus()){
                                email_refer.setText("");
                                refer_popup();
                            }


                            Toast.makeText(ReferAndEarnActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();





                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
    public void refer_popup() {
        final Dialog dialog = new Dialog(ReferAndEarnActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refer_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        Button done_btn=dialog.findViewById(R.id.done_btn);
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }



}