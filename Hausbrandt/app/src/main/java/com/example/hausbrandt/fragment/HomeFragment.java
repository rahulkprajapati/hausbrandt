package com.example.hausbrandt.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.MainActivity;
import com.example.hausbrandt.R;
import com.example.hausbrandt.activity.DashBoardActivity;
import com.example.hausbrandt.activity.NotificationActivity;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.model.ShopNearYouModel;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.pojo.ShopNearYouPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class HomeFragment extends Fragment implements OnMapReadyCallback {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private GoogleMap mMap;
    protected GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private boolean markerShow=true;
    private FusedLocationProviderClient fusedLocationClient;
    private ApiInterface apiInterface;
    private PrefsHelper prefsHelper;
    List<ShopNearYouModel> shopNearYouModelList=new ArrayList<>();

    ImageView notificaton_;

    MarkerOptions markerOptions;
    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        notificaton_ = view.findViewById(R.id.notificaton_);
        SupportMapFragment mapFragment = (SupportMapFragment)
                this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        prefsHelper=new PrefsHelper(getActivity());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        checkLocationPermission();




        notificaton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), NotificationActivity.class));

            }
        });


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("googleMap","true "+googleMap);
        mMap = googleMap;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        }
        else {
            fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);

        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                int position = (int)(marker.getTag());
                if(position==0){
                    if(shopNearYouModelList.size()>0){
                        shop_popup(shopNearYouModelList.get(0).getShopTitle(),shopNearYouModelList.get(0).getAddress(),shopNearYouModelList.get(0).getId(),shopNearYouModelList.get(0).getLatitude(),shopNearYouModelList.get(0).getLongitude());
                    }

                }else if(position==1){
                    if(shopNearYouModelList.size()>0){
                        shop_popup(shopNearYouModelList.get(1).getShopTitle(),shopNearYouModelList.get(1).getAddress(),shopNearYouModelList.get(1).getId(),shopNearYouModelList.get(1).getLatitude(),shopNearYouModelList.get(1).getLongitude());
                    }
                }else if(position==2){
                    if(shopNearYouModelList.size()>0){
                        shop_popup(shopNearYouModelList.get(2).getShopTitle(),shopNearYouModelList.get(2).getAddress(),shopNearYouModelList.get(2).getId(),shopNearYouModelList.get(2).getLatitude(),shopNearYouModelList.get(2).getLongitude());
                    }
                }
                //Using position get Value from arraylist
                return false;
            }
        });

        Log.d("fusedlocation",""+fusedLocationClient);

    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity) getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                    }

                } else {

                }
                return;
            }

        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                Constants.lat =location.getLatitude();
                Constants.lang = location.getLatitude();

                //Place current location marker
                if (markerShow){
                    markerShow=false;
                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                    MarkerOptions markerOptions = new MarkerOptions();
//                    markerOptions.position(latLng);
//                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));


                    getshops();

                }
               // connectWebSocket();
               // connectBooking();
                //move map camera

            }
        }
    };

    private void getshops() {

        CommonMethods.GetDialog(getActivity(),"Please wait","");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<ShopNearYouPojo> call3 = null;
        call3 = apiInterface.nearmeshopaddress("mo",mLastLocation.getLatitude(),mLastLocation.getLongitude(),"Bearer " + prefsHelper.getPref(Constants.authToken));
       // call3 = apiInterface.nearmeshopaddress("mo",mLastLocation.getLatitude(),mLastLocation.getLongitude(),"Bearer " + prefsHelper.getPref(Constants.authToken));
        assert call3 != null;
        call3.enqueue(new Callback<ShopNearYouPojo>() {
            @Override
            public void onResponse(Call<ShopNearYouPojo> call, retrofit2.Response<ShopNearYouPojo> response) {
                try {
                if(response.isSuccessful()){
                    if (response.body() == null) {
                        Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();
                    } else {
                        shopNearYouModelList=response.body().getData();
                        Log.d("RESs", "" + shopNearYouModelList.size());
                        if(shopNearYouModelList.size()>0){
                            for (int i=0;shopNearYouModelList.size()>0;i++){
                                LatLng latLng = new LatLng(Double.parseDouble(shopNearYouModelList.get(i).getLatitude()), Double.parseDouble(shopNearYouModelList.get(i).getLongitude()));
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                                mCurrLocationMarker = mMap.addMarker(markerOptions);
                                mCurrLocationMarker.setTag(i);

                            }

//                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//
////the include method will calculate the min and max bound.
//                            builder.include(mCurrLocationMarker.getPosition());
//                            LatLngBounds bounds = builder.build();
//                            int width = getResources().getDisplayMetrics().widthPixels;
//                            int height = getResources().getDisplayMetrics().heightPixels;
//                            int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen
//
//                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//                            mMap.moveCamera(cu);

                        }


                    }

                }else {
                    Toast.makeText(getActivity(), ""+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                }
            }catch (Exception ex){
                ex.toString();
                Log.d("RESs", "" + ex.toString());
            }
                CommonMethods.dismissDialog();
        }

        @Override
        public void onFailure(Call<ShopNearYouPojo> call, Throwable t) {
            call.cancel();
            CommonMethods.dismissDialog();
            Log.d("RESs", "" + t.toString());


        }
    });
}

    private void addFavourite(int shop_id, final Dialog dialog) {

        CommonMethods.GetDialog(getActivity(),"Please wait","");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<RegisterPojo> call3 = null;
        call3 = apiInterface.addfourite(shop_id,"Bearer " + prefsHelper.getPref(Constants.authToken));
        // call3 = apiInterface.nearmeshopaddress("mo",mLastLocation.getLatitude(),mLastLocation.getLongitude(),"Bearer " + prefsHelper.getPref(Constants.authToken));
        assert call3 != null;
        call3.enqueue(new Callback<RegisterPojo>() {
            @Override
            public void onResponse(Call<RegisterPojo> call, retrofit2.Response<RegisterPojo> response) {
                try {
                    if(response.isSuccessful()){
                        if (response.body() == null) {
                            Toast.makeText(getActivity(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if(response.body().getStatus()){
                                dialog.dismiss();
                                Toast.makeText(getActivity(), ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }else {
                        Toast.makeText(getActivity(), ""+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception ex){
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<RegisterPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    public void shop_popup(String shopname, String shopaddress, final int id, final String latitude, final String longitute) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.shop_details_popup);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        ImageView close_image=dialog.findViewById(R.id.close_image);
        Button add_favourite=dialog.findViewById(R.id.add_favourite);
        Button navigate_btn=dialog.findViewById(R.id.navigate_btn);
        TextView shop_name=dialog.findViewById(R.id.shop_name);
        shop_name.setText(""+shopname);
        TextView shop_address=dialog.findViewById(R.id.shop_address);
        shop_address.setText(""+shopaddress);
        close_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        add_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFavourite(id,dialog);
            }
        });

        navigate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    String url = "http://maps.google.com/maps?q=loc:" + String.format("%f,%f", Double.parseDouble(latitude), Double.parseDouble(longitute));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);

            }
        });
    }

}