package com.example.hausbrandt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hausbrandt.R;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteList;

import java.util.ArrayList;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.ViewHolder> {


    Context context;
    ArrayList<FavouriteList> stringArrayList;
    public FavouriteAdapter(Context context, ArrayList<FavouriteList> stringArrayList) {
        this.context =context;
    this.stringArrayList = stringArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.favourite_recyclerview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteAdapter.ViewHolder holder, int position) {


        holder.shop_name.setText(stringArrayList.get(position).getAddress() +" - "+stringArrayList.get(position).getCategory());
      //  holder.shop_name.setText(stringArrayList.get(position).getShopTitle());
      //  holder.abut_shop.setText(stringArrayList.get(position).getAddress() +" - "+stringArrayList.get(position).getCategory());


    }



    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView time,abut_shop,shop_name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            shop_name = itemView.findViewById(R.id.shop_name);
            abut_shop = itemView.findViewById(R.id.abut_shop);
            time = itemView.findViewById(R.id.time);
        }
    }
}
