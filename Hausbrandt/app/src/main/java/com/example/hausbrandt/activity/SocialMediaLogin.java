package com.example.hausbrandt.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.MainActivity;
import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.GPSTracker;
import com.example.hausbrandt.utils.InstagramApp;
import com.example.hausbrandt.utils.PrefsHelper;
import com.example.hausbrandt.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.hausbrandt.utils.Constants.CLIENT_ID;
import static com.example.hausbrandt.utils.Constants.CLIENT_SECRET;
import static com.example.hausbrandt.utils.InstagramApp.API_URL;
import static com.example.hausbrandt.utils.InstagramApp.AUTH_URL;
import static com.example.hausbrandt.utils.InstagramApp.TOKEN_URL;
import static com.example.hausbrandt.utils.InstagramApp.mCallbackUrl;

public class SocialMediaLogin extends AppCompatActivity  implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {



    ImageView back;
    TextView signup;


    LinearLayout fb_login,gmail_login,it_login;
    private GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;
    String FName ;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    String FEmail ;
    String GName ;
    String GEmail ;
    GPSTracker gpsTracker;

    String user_id;
    private InstagramApp mApp;
    private LinearLayout btnConnect;
          //  btnViewInfo;
    private LinearLayout llAfterLoginView;
//    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
//    private Handler handler = new Handler(new Handler.Callback() {
//
//        @Override
//        public boolean handleMessage(Message msg) {
//            if (msg.what == InstagramApp.WHAT_FINALIZE) {
//                userInfoHashmap = mApp.getUserInfo();
//            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
//                Toast.makeText(SocialMediaLogin.this, "Check your network.",
//                        Toast.LENGTH_SHORT).show();
//            }
//            return false;
//        }
//    });
    private GoogleSignInClient mGoogleSignInClient;
    private ApiInterface apiInterface;
    private String deviceUid;
    PrefsHelper prefsHelper;



    private String authURLFull;
    private String tokenURLFull;
    private String code;
    private String accessTokenString;
    private String dp;
    private String fullName;

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media_login);

        back = findViewById(R.id.back);
        it_login = findViewById(R.id.it_login);
        signup = findViewById(R.id.signup);
        fb_login = findViewById(R.id.fb_login);
        gmail_login = findViewById(R.id.gmail_login);
        it_login.setOnClickListener(this);
        fb_login.setOnClickListener(this);
        gmail_login.setOnClickListener(this);

        authURLFull = AUTH_URL
                + "?client_id="
                + CLIENT_ID
                + "&redirect_uri="
                + mCallbackUrl
                +"&scope=user_profile,user_media"
                + "&response_type=code";
        tokenURLFull = InstagramApp.TOKEN_URL + "?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&grant_type=authorization_code" + "&redirect_uri=" + mCallbackUrl ;


      //  tokenURLFull = TOKENURL + "?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&redirect_uri=" + REDIRECT_URI + "&grant_type=authorization_code";
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        prefsHelper = new PrefsHelper(SocialMediaLogin.this);

        gpsTracker=new GPSTracker(SocialMediaLogin.this);
        getDeviceModel();
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SocialMediaLogin.this, SignUpActivity.class));
                finish();
            }
        });


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        Log.v("LoginActivity Response ", response.toString());
                                        // Toast.makeText(context, "hello", Toast.LENGTH_SHORT).show();
                                        try {
                                            FName = object.getString("name");
                                            FEmail = object.getString("email");

                                            Submit(FName ,"",FEmail);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        System.out.println("login staus:" + " onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        // Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show();
                        System.out.println("login:" +exception.toString());
                    }
                });




        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

//        mApp = new InstagramApp(this, Constants.CLIENT_ID,
//                Constants.CLIENT_SECRET, Constants.CALLBACK_URL);
//        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
//
//            @Override
//            public void onSuccess() {
//                // tvSummary.setText("Connected as " + mApp.getUserName());
//             //   btnConnect.setText("Disconnect");
//            //    llAfterLoginView.setVisibility(View.VISIBLE);
//                //userInfoHashmap = mApp.
//                mApp.fetchUserName(handler);
//            }
//
//            @Override
//            public void onFail(String error) {
//                Toast.makeText(SocialMediaLogin.this, error, Toast.LENGTH_SHORT)
//                        .show();
//            }
//        });
//
//
//        if (mApp.hasAccessToken()) {
//            // tvSummary.setText("Connected as " + mApp.getUserName());
//          //  btnConnect.setText("Disconnect");
//            llAfterLoginView.setVisibility(View.VISIBLE);
//            mApp.fetchUserName(handler);
//
//        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fb_login:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email","public_profile"));
                break;

            case R.id.gmail_login:
                signIn();
                break;
                case R.id.it_login:
                    setupWebviewDialog(authURLFull);
                    CommonMethods.GetDialog(SocialMediaLogin.this, "Please wait", "");


                    //         connectOrDisconnectUser();
                break;
        }


//        } else if (v == btnViewInfo) {
//            displayInfoDialogView();
//        }displayInfoDialogView
    }

    

//    private void connectOrDisconnectUser() {
//        if (mApp.hasAccessToken()) {
//            final AlertDialog.Builder builder = new AlertDialog.Builder(
//                    SocialMediaLogin.this);
//            builder.setMessage("Disconnect from Instagram?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog,
//                                                    int id) {
//                                    mApp.resetAccessToken();
//                                    // btnConnect.setVisibility(View.VISIBLE);
//                                   // llAfterLoginView.setVisibility(View.GONE);
//                              //      btnConnect.setText("Connect");
//                                    // tvSummary.setText("Not connected");
//                                }
//                            })
//                    .setNegativeButton("No",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog,
//                                                    int id) {
//                                    dialog.cancel();
//                                }
//                            });
//            final AlertDialog alert = builder.create();
//            alert.show();
//        } else {
//            mApp.authorize();
//        }
//    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void signIn() {

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("RC_SIGN_IN",""+requestCode+"\n"+resultCode);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
           // int statusCode = result.getStatus().getStatusCode();
        }
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);//ERROR -- Code 10
            Log.d(TAG, "Account received");


            updateUI(account);
            Log.d(TAG, "updateUI Launched");
        } catch (ApiException e) {

            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        if (account!=null) {
            GEmail=account.getEmail();
            GName= account.getDisplayName();
            Submit(account.getGivenName(),account.getFamilyName(),account.getEmail());
            //tw1.setText("OK");
            Log.d("signInResult","Name: " + account.getGivenName() + ", Family name: " + account.getFamilyName() + ", Email: " + account.getEmail() /*+ " image: " +
                        account.getPhotoUrl()*/);
        }else {
           Log.d("signInResult","no");
        }

    }

    private void  Submit(String str_name,String str_sirname,String str_username) {
        CommonMethods.GetDialog(SocialMediaLogin.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
       //// JSONObject paramObject = new JSONObject();
        Call<LoginPojo> call3 = null;
        call3 = apiInterface.sociallogin(str_name,str_sirname,str_username,gpsTracker.getLatitude(),gpsTracker.getLongitude(),1,deviceUid);
        assert call3 != null;
        call3.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(SocialMediaLogin.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();

                                prefsHelper.savePref(Constants.authToken,response.body().getToken());
                                prefsHelper.savePref(Constants.name,response.body().getName());
                                prefsHelper.savePref(Constants.surname,response.body().getSurname());
                                prefsHelper.savePref(Constants.username,response.body().getUsername());
                                prefsHelper.savePref(Constants.email_id,response.body().getEmail());
                                prefsHelper.savePref(Constants.password,"");
                                prefsHelper.savePref(Constants.mobile,response.body().getMobileNumber());
                                prefsHelper.savePref(Constants.countrycode,response.body().getPhone_code());
                                if(response.body().getProfilePic()!=null && !response.body().getProfilePic().equals("")) {
                                    prefsHelper.savePref(Constants.profile, response.body().getProfilePic());
                                }
                                Toast.makeText(SocialMediaLogin.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SocialMediaLogin.this,DashBoardActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                //Toast.makeText(LoginActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                           //    finish();


                            } else {

                                Toast.makeText(SocialMediaLogin.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(SocialMediaLogin.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    @SuppressLint("HardwareIds")
    private void getDeviceModel() {
        //  deviceModel = android.os.Build.MODEL;
        deviceUid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

    }




    /*****  Show Instagram login page in a dialog *****************************/
    public void setupWebviewDialog(String url) {
        dialog = new Dialog(this);
        dialog.setTitle("Insta Login");

        WebView webView = new WebView(this);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setWebViewClient(new MyWVClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

        dialog.setContentView(webView);
    }

    /*****  A client to know about WebView navigations  ***********************/
    class MyWVClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            CommonMethods.dismissDialog();
            //   progressBar.setVisibility(View.VISIBLE);
           // progressBar.bringToFront();
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (request.getUrl().toString().startsWith(mCallbackUrl)) {
                handleUrl(request.getUrl().toString());
                return true;
            }
            return false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(mCallbackUrl)) {
                handleUrl(url);
                return true;
            }
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            dialog.show();
       //     CommonMethods.dismissDialog();

            //     progressBar.setVisibility(View.INVISIBLE);

        }
    }

    /*****  Check webview url for access token code or error ******************/
    public void handleUrl(String url) {

        if (url.contains("code")) {
            String temp[] = url.split("=");
            code = temp[1];
            String newcode = code.replace("#_","");
            new MyAsyncTask(newcode).execute();

        } else if (url.contains("error")) {
            String temp[] = url.split("=");
            Log.e(TAG, "Login error: "+temp[temp.length - 1]);
        }
    }

    /*****  AsyncTast to get user details after successful authorization ******/
    public class MyAsyncTask extends AsyncTask<URL, Integer, Long> {
        String code;

        public MyAsyncTask(String code) {
            this.code = code;
        }

        protected void onPreExecute() {
            CommonMethods.GetDialog(SocialMediaLogin.this, "Please wait", "");

            //    progressBar.setVisibility(View.VISIBLE);
        }

        protected Long doInBackground(URL... urls) {
            long result = 0;

            try {
                URL url = new URL(tokenURLFull);
             //   URL url = new URL(tokenURLFull);
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                httpsURLConnection.setRequestMethod("POST");
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpsURLConnection.getOutputStream());
                outputStreamWriter.write("client_id=" + CLIENT_ID +
                        "&client_secret=" + CLIENT_SECRET +
                        "&grant_type=authorization_code" +
                        "&redirect_uri=" + mCallbackUrl +
                        "&code=" + code);

                outputStreamWriter.flush();
                String response = streamToString(httpsURLConnection.getInputStream());
                JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                accessTokenString = jsonObject.getString("access_token"); //Here is your ACCESS TOKEN
                 user_id = jsonObject.getString("user_id"); //Here is your ACCESS TOKEN
             //   dp = jsonObject.getJSONObject("user").getString("profile_picture");
             //   fullName = jsonObject.getJSONObject("user").getString("full_name"); //This is how you can get the user info. You can explore the JSON sent by Instagram as well to know what info you got in a response
              //  fullName = jsonObject.getJSONObject("user").getString("username"); //This is how you can get the user info. You can explore the JSON sent by Instagram as well to know what info you got in a response
                URL urll = new URL(API_URL + "/"+user_id+"?" + "fields=id,username&access_token=" + accessTokenString);

                Log.d(TAG, "Opening URL " + urll.toString());
                HttpURLConnection urlConnection = (HttpURLConnection) urll
                        .openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.connect();
                String responses = Utils.streamToString(urlConnection
                        .getInputStream());
                System.out.println(responses);
                JSONObject jsonObj = (JSONObject) new JSONTokener(responses)
                        .nextValue();

                user_id = jsonObj.getString("username");
                fullName = jsonObj.getString("username");


//                spEdit = spUser.edit();
//                spEdit.putString(SP_TOKEN, accessTokenString);
//                spEdit.putString(SP_NAME, fullName);
//                spEdit.putString(SP_DP, dp);
//                spEdit.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(Long result) {
            dialog.dismiss();

            Submit(fullName ,"",user_id);


        }
    }



    /*****  Converting stream to string ***************************************/
    public static String streamToString(InputStream is) throws IOException {
        String str = "";

        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();

            } finally {
                is.close();
            }
            str = sb.toString();
        }
        return str;
    }

    private boolean isLoggedIn(){
        //  String token = spUser.getString(SP_TOKEN, null);
//        if (token != null){
//            return true;
//        }
        return false;
    }
}


