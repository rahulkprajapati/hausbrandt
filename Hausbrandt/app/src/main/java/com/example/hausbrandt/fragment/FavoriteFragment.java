package com.example.hausbrandt.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.activity.NotificationActivity;
import com.example.hausbrandt.adapter.FavouriteAdapter;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.aboutModel.AboutResponse;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteList;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteResponse;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.hausbrandt.R.string.please_add_favorite_shop;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    PrefsHelper prefsHelper;
    FavouriteAdapter favouriteAdapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView faavourite_recyclerView;
    ArrayList<FavouriteList>  stringArrayList;

ImageView notification;
    public FavoriteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoriteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoriteFragment newInstance(String param1, String param2) {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        prefsHelper = new PrefsHelper(getContext());
        stringArrayList = new ArrayList<>();

        faavourite_recyclerView = view.findViewById(R.id.favorite_recylerview);
        linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        faavourite_recyclerView.setLayoutManager(linearLayoutManager);
        favouriteAdapter = new FavouriteAdapter(getContext(), stringArrayList);
        faavourite_recyclerView.setAdapter(favouriteAdapter);
        favouriteAdapter.notifyDataSetChanged();

        notification = view.findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), NotificationActivity.class));

            }
        });
        get_favourite();
        return view;
    }



    private void get_favourite() {
        CommonMethods.GetDialog(getContext(), "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<FavouriteResponse> call3 = null;
        //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.get_fav(String.valueOf(prefsHelper.getPref(Constants.language)),  token);
        assert call3 != null;
        call3.enqueue(new Callback<FavouriteResponse>() {
            @Override
            public void onResponse(Call<FavouriteResponse> call, retrofit2.Response<FavouriteResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(getContext(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {

                            if(response.body().getList().size()>0)
                            {
                                stringArrayList.clear();
                                stringArrayList = response.body().getList();
                                favouriteAdapter = new FavouriteAdapter(getContext(), stringArrayList);
                                faavourite_recyclerView.setAdapter(favouriteAdapter);
                                favouriteAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                Toast.makeText(getContext(), please_add_favorite_shop, Toast.LENGTH_SHORT).show();
                            }






                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<FavouriteResponse> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
}