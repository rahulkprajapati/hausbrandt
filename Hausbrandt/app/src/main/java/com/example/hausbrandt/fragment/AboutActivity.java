package com.example.hausbrandt.fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.activity.NotificationActivity;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.LoginPojo;
import com.example.hausbrandt.pojo.aboutModel.AboutResponse;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class AboutActivity extends Fragment {


    private String mParam1;
    private String mParam2;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    PrefsHelper prefsHelper;


    TextView title,description;
    ImageView notification;
    public AboutActivity() {
        // Required empty public constructor
    }


    public static AboutActivity newInstance(String param1, String param2) {
        AboutActivity fragment = new AboutActivity();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_about, container, false);

        prefsHelper = new PrefsHelper(getContext());
        description = view.findViewById(R.id.description);
        title = view.findViewById(R.id.title);
        notification = view.findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), NotificationActivity.class));

            }
        });
        get_about();

        return view;
    }



    private void get_about() {
        CommonMethods.GetDialog(getContext(), "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<AboutResponse> call3 = null;
         //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.get_about(String.valueOf(prefsHelper.getPref(Constants.language)), "5", token);
        assert call3 != null;
        call3.enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, retrofit2.Response<AboutResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(getContext(), "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {



                            title.setText(response.body().getData().getTitle());
                            description.setText(Html.fromHtml(response.body().getData().getDescription()));




                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
    }