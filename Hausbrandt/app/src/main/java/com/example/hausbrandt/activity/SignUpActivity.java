package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.RegisterPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.GPSTracker;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

   private TextView login;
   private EditText edt_name,edt_sirname,edt_username,edt_email,edt_mobile,edt_password,edt_con_password,reffer_code;
   private String str_name,str_sirname,str_username,str_email,str_mobile,str_password,str_con_password,str_reffer_code;
   private Button btn_signup;
    private ApiInterface apiInterface;
    GPSTracker gpsTracker;
    private String deviceUid;
    private CountryCodePicker countryCodePicker;
    String country_code;

    CheckBox checkbox_term;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        edt_name = findViewById(R.id.edt_name);
        reffer_code = findViewById(R.id.reffer_code);
        edt_sirname = findViewById(R.id.edt_sirname);
        edt_username = findViewById(R.id.edt_username);
        edt_email = findViewById(R.id.edt_email);
        edt_mobile = findViewById(R.id.edt_mobile);
        edt_password = findViewById(R.id.edt_password);
        edt_con_password = findViewById(R.id.edt_con_password);
        btn_signup = findViewById(R.id.btn_signup);
        countryCodePicker = findViewById(R.id.country_code);
        login = findViewById(R.id.login);
        checkbox_term = findViewById(R.id.checkbox_term);
        login.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        gpsTracker=new GPSTracker(SignUpActivity.this);
        country_code=countryCodePicker.getSelectedCountryCode();
        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                country_code=countryCodePicker.getSelectedCountryCode();
                Log.d("country_code",country_code);
            }
        });
        getDeviceModel();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btn_signup:
                getText();
                if(str_name.equalsIgnoreCase("")){
                    edt_name.setError("Please enter name");
                }else if(str_sirname.equalsIgnoreCase("")){
                    edt_sirname.setError("Please enter sirname");
                }else if(str_username.equalsIgnoreCase("")){
                    edt_username.setError("Please enter username");
                }else if(str_email.equalsIgnoreCase("")){
                    edt_email.setError("Please enter email");
                }else if(str_mobile.equalsIgnoreCase("")){
                    edt_mobile.setError("Please enter phone number");
                }else if(str_password.equalsIgnoreCase("")){
                    edt_password.setError("Please enter password");
                }else if(str_con_password.equalsIgnoreCase("")){
                    edt_con_password.setError("Please enter password");
                }else if(!str_con_password.equalsIgnoreCase(str_password)) {
                    edt_con_password.setError("Confirm password not matched");
                }
                    else if(!checkbox_term.isChecked()){
                    checkbox_term.setError("Please Select Tearm & Condition");
                }else {
                    Submit();
                }
                break;
            case R.id.login:
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
                break;

        }
    }

    private void getText(){
        str_name=edt_name.getText().toString();
        str_sirname=edt_sirname.getText().toString();
        str_username=edt_username.getText().toString();
        str_email=edt_email.getText().toString();
        str_mobile=edt_mobile.getText().toString();
        str_password=edt_password.getText().toString();
        str_con_password=edt_con_password.getText().toString();
        str_reffer_code=reffer_code.getText().toString();
    }

    private void  Submit() {
        CommonMethods.GetDialog(SignUpActivity.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<RegisterPojo> call3 = null;
        call3 = apiInterface.register(str_name,str_sirname,str_username,str_email,str_mobile,str_password,gpsTracker.getLatitude(),gpsTracker.getLongitude(),1,deviceUid,country_code,str_reffer_code );
        assert call3 != null;
        call3.enqueue(new Callback<RegisterPojo>() {
            @Override
            public void onResponse(Call<RegisterPojo> call, retrofit2.Response<RegisterPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(SignUpActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                //  thankYouPopup();
                                Toast.makeText(SignUpActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {

                                Toast.makeText(SignUpActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(SignUpActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<RegisterPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    @SuppressLint("HardwareIds")
    private void getDeviceModel() {
      //  deviceModel = android.os.Build.MODEL;
        deviceUid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//        prefsHelper = new PrefsHelper(Password.this);
//        deviceToken = prefsHelper.getPref(Constants.DEVICE_TOKEN);
//        if(prefsHelper.getPref(Constants.DEVICE_TOKEN)==null){
//            deviceToken= FirebaseInstanceId.getInstance().getToken();
//        }
//
//        Log.d("DEVICETOKEN", "PASSWORD" + deviceToken);
//
//        deviceversion = Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
//        RELEASE =  Build.VERSION.RELEASE;
//        model =  Build.MODEL;
//
//        System.out.println("==== device version : " + deviceversion + " " + "Release :" + RELEASE + " " + "Model : " + model +" " + "deviceModel : " + deviceModel);

    }
}