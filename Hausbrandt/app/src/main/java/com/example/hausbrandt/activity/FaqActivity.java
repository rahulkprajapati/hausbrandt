package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.adapter.FaqAdapterSecond;
import com.example.hausbrandt.adapter.FavouriteAdapter;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.faq.FaqDatum;
import com.example.hausbrandt.pojo.faq.FaqResponse;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteList;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteResponse;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class FaqActivity extends AppCompatActivity {


    PrefsHelper prefsHelper;
    FaqAdapterSecond favouriteAdapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView faavourite_recyclerView;
    ArrayList<FaqDatum>  stringArrayList;

    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);


        prefsHelper = new PrefsHelper(this);
        stringArrayList = new ArrayList<>();
        faavourite_recyclerView = findViewById(R.id.faq_recylerview);
        back = findViewById(R.id.back);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        faavourite_recyclerView.setLayoutManager(linearLayoutManager);
        favouriteAdapter = new FaqAdapterSecond(this, faavourite_recyclerView);
        faavourite_recyclerView.setAdapter(favouriteAdapter);
        favouriteAdapter.notifyDataSetChanged();

        favouriteAdapter.setDataFaqList(stringArrayList);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        get_favourite();

    }


    private void get_favourite() {
        CommonMethods.GetDialog(this, "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<FaqResponse> call3 = null;
        //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.get_faq(String.valueOf(prefsHelper.getPref(Constants.language)),  token);
        assert call3 != null;
        call3.enqueue(new Callback<FaqResponse>() {
            @Override
            public void onResponse(Call<FaqResponse> call, retrofit2.Response<FaqResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(FaqActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {

                            if(response.body().getData().size()>0)
                            {
                                stringArrayList.clear();
                                stringArrayList = response.body().getData();
                                favouriteAdapter = new FaqAdapterSecond(FaqActivity.this, faavourite_recyclerView);
                                faavourite_recyclerView.setAdapter(favouriteAdapter);
                                favouriteAdapter.notifyDataSetChanged();

                                favouriteAdapter.setDataFaqList(stringArrayList);
                            }
                            else
                            {
                                Toast.makeText(FaqActivity.this, "Please Add FAQ of shops", Toast.LENGTH_SHORT).show();
                            }






                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<FaqResponse> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

}