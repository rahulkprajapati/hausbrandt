
package com.example.hausbrandt.pojo.favouriteModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FavouriteResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("list")
    @Expose
    private ArrayList<FavouriteList> list = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<FavouriteList> getList() {
        return list;
    }

    public void setList(ArrayList<FavouriteList> list) {
        this.list = list;
    }

}
