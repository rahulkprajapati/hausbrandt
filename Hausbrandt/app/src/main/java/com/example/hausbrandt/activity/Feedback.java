package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.ForgotPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import retrofit2.Call;
import retrofit2.Callback;

public class Feedback extends AppCompatActivity implements View.OnClickListener {

    ImageView imageView1,imageView2,imageView3,imageView4,imageView5,back;
    private String image_count="0";
    private ApiInterface apiInterface;
    private EditText report_edt;
    private String report_str;
    private Button submit_btn;
    private PrefsHelper prefsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
          imageView1=findViewById(R.id.coffee1);
          imageView2=findViewById(R.id.coffee2);
          imageView3=findViewById(R.id.coffee3);
          imageView4=findViewById(R.id.coffee4);
          imageView5=findViewById(R.id.coffee5);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        report_edt=findViewById(R.id.report_edt);
        submit_btn=findViewById(R.id.submit_btn);
        prefsHelper=new PrefsHelper(Feedback.this);
        submit_btn.setOnClickListener(this);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean_grey);
                imageView3.setImageResource(R.drawable.coffee_bean_grey);
                imageView4.setImageResource(R.drawable.coffee_bean_grey);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "1";
            }
        });


        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean_grey);
                imageView4.setImageResource(R.drawable.coffee_bean_grey);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "2";
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean);
                imageView4.setImageResource(R.drawable.coffee_bean_grey);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "3";
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean);
                imageView4.setImageResource(R.drawable.coffee_bean);
                imageView5.setImageResource(R.drawable.coffee_bean_grey);
                image_count = "4";
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imageView1.setImageResource(R.drawable.coffee_bean);
                imageView2.setImageResource(R.drawable.coffee_bean);
                imageView3.setImageResource(R.drawable.coffee_bean);
                imageView4.setImageResource(R.drawable.coffee_bean);
                imageView5.setImageResource(R.drawable.coffee_bean);
                image_count = "5";
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_btn:
                getText();
                if(report_str.equalsIgnoreCase("")){
                    report_edt.setError("Please enter required field");
                }else {
                    Submit();
                }

                break;

            case R.id.back:
                finish();
                break;
        }
    }

    private void getText(){
        report_str=report_edt.getText().toString();
    }

    private void  Submit() {
        CommonMethods.GetDialog(Feedback.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        // JSONObject paramObject = new JSONObject();
        Call<ForgotPojo> call3 = null;
        call3 = apiInterface.feedback_send(report_str,image_count,"Bearer " + prefsHelper.getPref(Constants.authToken));
        assert call3 != null;
        call3.enqueue(new Callback<ForgotPojo>() {
            @Override
            public void onResponse(Call<ForgotPojo> call, retrofit2.Response<ForgotPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(Feedback.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                Toast.makeText(Feedback.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {

                                Toast.makeText(Feedback.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(Feedback.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<ForgotPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
}