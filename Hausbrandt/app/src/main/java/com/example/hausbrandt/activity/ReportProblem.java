package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.ForgotPojo;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

public class ReportProblem extends AppCompatActivity implements View.OnClickListener {

    private ApiInterface apiInterface;
    private EditText report_edt;
    private String report_str;
    private Button submit_btn;
    private PrefsHelper prefsHelper;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);
        report_edt=findViewById(R.id.report_edt);
        submit_btn=findViewById(R.id.submit_btn);
        prefsHelper=new PrefsHelper(ReportProblem.this);
        submit_btn.setOnClickListener(this);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
    }

    private void  Submit() {
        CommonMethods.GetDialog(ReportProblem.this, "Please wait", "");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
       // JSONObject paramObject = new JSONObject();
        Call<ForgotPojo> call3 = null;
        call3 = apiInterface.report_send(report_str,"Bearer " + prefsHelper.getPref(Constants.authToken));
        assert call3 != null;
        call3.enqueue(new Callback<ForgotPojo>() {
            @Override
            public void onResponse(Call<ForgotPojo> call, retrofit2.Response<ForgotPojo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(ReportProblem.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {
                            if (response.body().getStatus()) {
                                Toast.makeText(ReportProblem.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {

                                Toast.makeText(ReportProblem.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else {
                        Toast.makeText(ReportProblem.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<ForgotPojo> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_btn:
                getText();
                if(report_str.equalsIgnoreCase("")){
                    report_edt.setError("Please enter required field");
                }else {
                    Submit();
                }

                break;
                case R.id.back:
                finish();
                break;
        }
    }

    private void getText(){
        report_str=report_edt.getText().toString();
    }
}