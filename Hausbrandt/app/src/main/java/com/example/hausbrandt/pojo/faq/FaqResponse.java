
package com.example.hausbrandt.pojo.faq;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaqResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<FaqDatum> data = null;

    public ArrayList<FaqDatum> getData() {
        return data;
    }

    public void setData(ArrayList<FaqDatum> data) {
        this.data = data;
    }

}
