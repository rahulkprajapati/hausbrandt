package com.example.hausbrandt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hausbrandt.R;
import com.example.hausbrandt.adapter.FaqAdapterSecond;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.aboutModel.AboutResponse;
import com.example.hausbrandt.pojo.faq.FaqDatum;
import com.example.hausbrandt.pojo.faq.FaqResponse;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class PrivacyPolicyActivity extends AppCompatActivity {


    PrefsHelper prefsHelper;

    ImageView back;
    TextView title_activity,description,title;
    String code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);


        prefsHelper = new PrefsHelper(this);
        back = findViewById(R.id.back);
        title_activity = findViewById(R.id.title_activity);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Intent ii = getIntent();

        if(ii!=null)
        {

            code = ii.getStringExtra("code");

            if(code.equals("6"))
            {
                title_activity.setText(R.string.pp);
            }
            else if(code.equals("7"))
            {
                title_activity.setText(R.string.tc);
            }

            else if(code.equals("8"))
            {
                title_activity.setText(R.string.bbb);
            }
            else if(code.equals("9"))
            {
                title_activity.setText(R.string.ruleofgame);
            }

        }

        get_data(code);


    }


    private void get_data(String code) {
        CommonMethods.GetDialog(this, "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<AboutResponse> call3 = null;
        //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.get_about(String.valueOf(prefsHelper.getPref(Constants.language)), code, token);
        assert call3 != null;
        call3.enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, retrofit2.Response<AboutResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(PrivacyPolicyActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {



                            title.setText(response.body().getData().getTitle());
                            description.setText(Html.fromHtml(response.body().getData().getDescription()));




                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
}
