package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hausbrandt.R;
import com.example.hausbrandt.adapter.FavouriteAdapter;
import com.example.hausbrandt.adapter.NoticationAdapter;
import com.example.hausbrandt.api.ApiClient;
import com.example.hausbrandt.interfaces.ApiInterface;
import com.example.hausbrandt.pojo.NotificationData;
import com.example.hausbrandt.pojo.NotificationResponse;
import com.example.hausbrandt.pojo.favouriteModel.FavouriteResponse;
import com.example.hausbrandt.utils.CommonMethods;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.PrefsHelper;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.hausbrandt.R.string.not_found_notification;

public class NotificationActivity extends AppCompatActivity {


    NoticationAdapter favouriteAdapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView notification_recyclerview;
    ArrayList<NotificationData> stringArrayList;
    PrefsHelper prefsHelper;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        prefsHelper = new PrefsHelper(this);
        stringArrayList = new ArrayList<>();
        back = findViewById(R.id.back);
        notification_recyclerview = findViewById(R.id.notification_recyclerview);
        linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        notification_recyclerview.setLayoutManager(linearLayoutManager);
        favouriteAdapter = new NoticationAdapter(this, stringArrayList);
        notification_recyclerview.setAdapter(favouriteAdapter);
        favouriteAdapter.notifyDataSetChanged();


        get_nnotification();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    private void get_nnotification()
    {
        CommonMethods.GetDialog(NotificationActivity.this, "Please wait", "");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        Call<NotificationResponse> call3 = null;
        //   RequestBody token = RequestBody.create(MediaType.parse("text/plain"), "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken)));
        String token = "Bearer "+String.valueOf(prefsHelper.getPref(Constants.authToken));

        call3 = apiInterface.get_notification(  token);
        assert call3 != null;
        call3.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() == null) {
                            Toast.makeText(NotificationActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
                        } else {

                            if(response.body().getNotification().size()>0)
                            {
                                stringArrayList.clear();
                                stringArrayList = response.body().getNotification();
                                favouriteAdapter = new NoticationAdapter(NotificationActivity.this, stringArrayList);
                                notification_recyclerview.setAdapter(favouriteAdapter);
                                favouriteAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                Toast.makeText(NotificationActivity.this, not_found_notification, Toast.LENGTH_SHORT).show();
                            }







                        }

                    } else {
                        //Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception ex) {
                    ex.toString();
                    Log.d("RESs", "" + ex.toString());
                }
                CommonMethods.dismissDialog();
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                call.cancel();
                CommonMethods.dismissDialog();
                Log.d("RESs", "" + t.toString());


            }
        });
    }
}