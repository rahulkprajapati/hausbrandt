package com.example.hausbrandt.pojo.aboutModel;

import com.example.hausbrandt.model.ShopNearYouModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AboutResponse {


    @SerializedName("data")
    @Expose
    private AboutData data = null;

    public AboutData getData() {
        return data;
    }

    public void setData(AboutData data) {
        this.data = data;
    }
}
