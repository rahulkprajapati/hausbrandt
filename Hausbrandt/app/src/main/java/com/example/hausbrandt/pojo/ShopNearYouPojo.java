package com.example.hausbrandt.pojo;

import com.example.hausbrandt.model.ShopNearYouModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ShopNearYouPojo {
    @SerializedName("data")
    @Expose
    private ArrayList<ShopNearYouModel> data = null;

    public ArrayList<ShopNearYouModel> getData() {
        return data;
    }

    public void setData(ArrayList<ShopNearYouModel> data) {
        this.data = data;
    }
}
