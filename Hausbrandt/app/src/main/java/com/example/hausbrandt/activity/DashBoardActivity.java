package com.example.hausbrandt.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hausbrandt.R;
import com.example.hausbrandt.fragment.AboutActivity;
import com.example.hausbrandt.fragment.EditProfileActivity;
import com.example.hausbrandt.fragment.FavoriteFragment;
import com.example.hausbrandt.fragment.HomeFragment;
import com.example.hausbrandt.fragment.UploadRecieptFragment;
import com.example.hausbrandt.utils.Constants;
import com.example.hausbrandt.utils.LocaleHelper;
import com.example.hausbrandt.utils.PrefsHelper;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener{


    LinearLayout profile,favourite,beans_treasure,about,refer_friend,faq,privacy_policy,beans_by_beans,tearm_condition;
    public static DashBoardActivity instance;


    ImageView humburger;
    DrawerLayout drawer_layout;
    LinearLayout logout,customer_level,setting;
    TextView name;
    final FragmentManager fm=getSupportFragmentManager();
    final Fragment fragmentHome=new HomeFragment();
    final Fragment UploadRecieptFragment=new UploadRecieptFragment();
    final EditProfileActivity editProfileActivity=new EditProfileActivity();
    final FavoriteFragment favoriteFragment=new FavoriteFragment();
    final AboutActivity aboutActivity=new AboutActivity();
    PrefsHelper prefsHelper;
    LinearLayout linear_home,linear_profile,global_click,favourate_linear,cup_linear,help;
    ImageView home_image,profile_image,global_image,favourite_image,cup_image;

    TextView aboutud_txt,
    beanybean_txt,
    profile_txt,
    favirote_txt,
    beans_teausr_txt,
    custmore_txt,
    refer_earn_txt,
    setting_txt,
    faq_txt,
    help_txt,
    logout_txt;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_layout);
        instance=this;
        prefsHelper = new PrefsHelper(this);
        profile = findViewById(R.id.profile);
        help = findViewById(R.id.help);
        favourite = findViewById(R.id.favourite);
        humburger = findViewById(R.id.humburger);
        drawer_layout = findViewById(R.id.drawer_layout);
        logout = findViewById(R.id.logout);
        name = findViewById(R.id.name);
        linear_home = findViewById(R.id.linear_home);
        linear_home.setOnClickListener(this);
        linear_profile = findViewById(R.id.linear_profile);
        linear_profile.setOnClickListener(this);
        global_click = findViewById(R.id.global_click);
        global_click.setOnClickListener(this);
        favourate_linear = findViewById(R.id.favourate_linear);
        favourate_linear.setOnClickListener(this);
        cup_linear = findViewById(R.id.cup_linear);
        cup_linear.setOnClickListener(this);
        cup_image = findViewById(R.id.cup_image);
        favourite_image = findViewById(R.id.favourite_image);
        global_image = findViewById(R.id.global_image);
        profile_image = findViewById(R.id.profile_image);
        home_image = findViewById(R.id.home_image);
        customer_level = findViewById(R.id.customer_level);
        beans_treasure = findViewById(R.id.beans_treasure);
        about = findViewById(R.id.about);
        setting = findViewById(R.id.setting);
        refer_friend = findViewById(R.id.refer_friend);
        faq = findViewById(R.id.faq);
        beans_by_beans = findViewById(R.id.beans_by_beans);
        privacy_policy = findViewById(R.id.privacy_policy);
        tearm_condition = findViewById(R.id.tearm_condition);

        aboutud_txt = findViewById(R.id.aboutud_txt);
        beanybean_txt = findViewById(R.id.beanybean_txt);
        profile_txt = findViewById(R.id.profile_txt);
        favirote_txt = findViewById(R.id.favirote_txt);
        beans_teausr_txt = findViewById(R.id.beans_teausr_txt);
        custmore_txt = findViewById(R.id.custmore_txt);
        refer_earn_txt = findViewById(R.id.refer_earn_txt);
        setting_txt = findViewById(R.id.setting_txt);
        faq_txt = findViewById(R.id.faq_txt);
        help_txt = findViewById(R.id.help_txt);
        logout_txt = findViewById(R.id.logout_txt);





        if(prefsHelper.getPref(Constants.language,null)!=null)
        {
            if(prefsHelper.getPref(Constants.language,null).equals("en"))
            {
                LocaleHelper.setLocale(DashBoardActivity.this, "en");
                prefsHelper.savePref(Constants.language,"en");
            }
            else
            {
                LocaleHelper.setLocale(DashBoardActivity.this, "hi");
                prefsHelper.savePref(Constants.language,"mo");
            }

        }
        else
        {
            LocaleHelper.setLocale(DashBoardActivity.this, "en");
            prefsHelper.savePref(Constants.language,"en");
        }


                name.setText(getString(R.string.hello) +String.valueOf(prefsHelper.getPref(Constants.name)));

         aboutud_txt.setText(getResources().getText(R.string.about));
        beanybean_txt.setText(getResources().getText(R.string.beans_by_beans));
        profile_txt.setText(getResources().getText(R.string.profile));
        favirote_txt.setText(getResources().getText(R.string.favorite));
        beans_teausr_txt.setText(getResources().getText(R.string.bean_treasure));
        custmore_txt.setText(getResources().getText(R.string.customer_level));
        refer_earn_txt.setText(getResources().getText(R.string.refer_amp_earn));
        setting_txt.setText(getResources().getText(R.string.setting));
        faq_txt.setText(getResources().getText(R.string.faq));
        help_txt.setText(getResources().getText(R.string.help));
         logout_txt.setText(getResources().getText(R.string.logout));



        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                drawer_layout.closeDrawer(GravityCompat.START);
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(aboutActivity,"about");

            }
        });



        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, SettingActivity.class));
            }
        });

          help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, HelpActivity.class));
            }
        });

        refer_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, ReferAndEarnActivity.class));
            }
        });

         beans_by_beans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, PrivacyPolicyActivity.class)
                .putExtra("code","8"));
            }
        });

          privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, PrivacyPolicyActivity.class)
                .putExtra("code","6"));
            }
        });
           tearm_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, PrivacyPolicyActivity.class)
                .putExtra("code","7"));
            }
        });



        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);

                startActivity(new Intent(DashBoardActivity.this, FaqActivity.class));
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer_layout.closeDrawer(GravityCompat.START);
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(editProfileActivity,"profile");

            }
        });


        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(favoriteFragment,"favourite");

            }
        });

        beans_treasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                add(UploadRecieptFragment,"reciept");
            }
        });


        humburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                drawer_layout.openDrawer(GravityCompat.START);

            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);


                prefsHelper.clearAllPref();
                startActivity(new Intent(DashBoardActivity.this, LoginActivity.class));
                finish();
            }
        });

        customer_level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawer(GravityCompat.START);


                startActivity(new Intent(DashBoardActivity.this, CustomerLevelActivity.class));

            }
        });


        home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
        profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
        global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
        favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
        cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));

        add(fragmentHome,"Home");
    }



    public static DashBoardActivity getInstance() {
        return instance;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void add(Fragment fragment1, String tag){
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        Fragment curFrag = fm.getPrimaryNavigationFragment();
        if (curFrag != null) {
            fragmentTransaction.detach(curFrag);
        }

        Fragment fragment = fm.findFragmentByTag(tag);
        if (fragment == null) {
            fragment = fragment1;
            fragmentTransaction.add(R.id.pre_fram_layout, fragment, tag);
        } else {
            fragmentTransaction.attach(fragment);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragment);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.linear_home:
                Log.d("linear_home","yes");
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(fragmentHome,"Home");
                break;
            case R.id.linear_profile:
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(editProfileActivity,"profile");
                break;
            case R.id.global_click:
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(aboutActivity,"about");
                break;
            case R.id.favourate_linear:
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                add(favoriteFragment,"favourite");
                break;
            case R.id.cup_linear:
                home_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                profile_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                global_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                favourite_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.white));
                cup_image.setColorFilter(ContextCompat.getColor(DashBoardActivity.this,R.color.colorPrimary));
                add(UploadRecieptFragment,"reciept");
                break;
        }
    }
}